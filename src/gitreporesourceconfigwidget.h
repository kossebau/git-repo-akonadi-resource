/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef GITREPORESOURCECONFIGWIDGET_H
#define GITREPORESOURCECONFIGWIDGET_H

#include "ui_gitreporesourceconfigwidget.h"

#include <AkonadiCore/AgentConfigurationBase>

class KConfigDialogManager;

class GitRepoResourceConfigWidget : public Akonadi::AgentConfigurationBase
{
    Q_OBJECT

public:
    explicit GitRepoResourceConfigWidget(const KSharedConfigPtr &config, QWidget *parent, const QVariantList &args);

    void load() override;
    bool save() const override;

private:
    void validate();

private:
    Ui::GitRepoResourceConfigWidget ui;
    KConfigDialogManager *mManager = nullptr;
};

#endif
