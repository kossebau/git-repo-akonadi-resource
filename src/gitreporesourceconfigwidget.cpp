/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "gitreporesourceconfigwidget.h"

#include "settings.h"

#include <KConfigDialogManager>
#include <KLocalizedString>

#include <QUrl>
#include <QPushButton>
#include <QTimer>

#include <git2.h>

AKONADI_AGENTCONFIG_FACTORY(GitRepoResourceConfigWidgetFactory, "gitreporesourceconfig.json",
                            GitRepoResourceConfigWidget)

GitRepoResourceConfigWidget::GitRepoResourceConfigWidget(const KSharedConfigPtr &config,
                                                         QWidget *parent, const QVariantList &args)
    : Akonadi::AgentConfigurationBase(config, parent, args)
{
    Settings::instance(config);

    auto *mainWidget = new QWidget(parent);

    ui.setupUi(mainWidget);
    parent->layout()->addWidget(mainWidget);

    ui.kcfg_Path->setMode(KFile::LocalOnly | KFile::Directory);

    connect(ui.kcfg_Path, &KUrlRequester::textChanged, this, &GitRepoResourceConfigWidget::validate);

    ui.kcfg_Path->setUrl(QUrl::fromLocalFile(Settings::self()->path()));
    mManager = new KConfigDialogManager(mainWidget, Settings::self());
}

void GitRepoResourceConfigWidget::validate()
{
    const QUrl currentUrl = ui.kcfg_Path->url();

    git_repository * repo = nullptr;

    if (!currentUrl.isEmpty()) {
        // try to create a repo
        git_repository_open(&repo, currentUrl.toLocalFile().toLocal8Bit().constData());
        if (!repo) {
            ui.locationValidWidget->setMessageType(KMessageWidget::Error);
            ui.locationValidWidget->setText(i18n("No git repository detected."));
            Q_EMIT enableOkButton(false);
            return;
        }
        // clean up
        git_repository_free(repo);
    }

    ui.locationValidWidget->setMessageType(KMessageWidget::Positive);
    ui.locationValidWidget->setText(i18n("Git repository detected."));
    Q_EMIT enableOkButton(true);
}

void GitRepoResourceConfigWidget::load()
{
    mManager->updateWidgets();
    QTimer::singleShot(0, this, &GitRepoResourceConfigWidget::validate);
}

bool GitRepoResourceConfigWidget::save() const
{
    mManager->updateSettings();
    Settings::self()->setPath(ui.kcfg_Path->url().toLocalFile());
    Settings::self()->save();
    return true;
}

#include "gitreporesourceconfigwidget.moc"
