/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "gitreporesource.h"

#include "settings.h"
#include "settingsadaptor.h"
#include "gitreporesource_log.h"

#include <AkonadiCore/Collection>
#include <AkonadiCore/EntityDisplayAttribute>

#include <KCalendarCore/Event>

#include <KLocalizedString>

#include <QDBusConnection>
#include <QDateTime>

#include <git2.h>

using namespace Akonadi;

GitRepoResource::GitRepoResource(const QString &id)
    : ResourceBase(id)
{
    Settings::instance(KSharedConfig::openConfig());
    new SettingsAdaptor(Settings::self());
    QDBusConnection::sessionBus().registerObject(QStringLiteral("/Settings"),
                                                 Settings::self(),
                                                 QDBusConnection::ExportAdaptors);

    connect(this, &GitRepoResource::reloadConfiguration, this, &GitRepoResource::reloadConfig);

    setName(i18n("Git Repo Events"));

    qCDebug(LOG_GITREPORESOURCE) << "GitRepoResource started";
    git_libgit2_init();
}

GitRepoResource::~GitRepoResource()
{
    git_libgit2_shutdown();
    qCDebug(LOG_GITREPORESOURCE) << "GitRepoResource destructed";
}

void GitRepoResource::reloadConfig()
{
    synchronize();
}

void GitRepoResource::retrieveCollections()
{
    Collection c;
    c.setParentCollection(Collection::root());
    c.setRemoteId(repoDirectoryPath());
    c.setName(name());
    c.setContentMimeTypes(QStringList{KCalendarCore::Event::eventMimeType()});
    c.setRights(Collection::ReadOnly);

    auto attribute = c.attribute<EntityDisplayAttribute>(Collection::AddIfMissing);
    attribute->setIconName(QStringLiteral("git"));

    const Collection::List list { c };
    qCDebug(LOG_GITREPORESOURCE) << "collections size" << list.size();
    collectionsRetrieved(list);
}

void GitRepoResource::retrieveItems(const Akonadi::Collection &collection)
{
    Q_UNUSED(collection);

    Akonadi::Item::List items;

    git_repository * repo = nullptr;
    git_repository_open(&repo, repoDirectoryPath().toLocal8Bit().constData());

    git_revwalk * walker = nullptr;
    git_revwalk_new(&walker, repo);
    git_revwalk_sorting(walker, GIT_SORT_NONE); // GIT_SORT_NONE is default, just for explicitness

    git_revwalk_push_head(walker);
//     items.reserve(mIncidences.count()); TODO: any chance to know in advance the number?

    git_oid oid;

    while(!git_revwalk_next(&oid, walker)) {
        Akonadi::Item item;
        item.setRemoteId(QString::fromLocal8Bit(git_oid_tostr_s(&oid), -1));
        item.setMimeType(KCalendarCore::Event::eventMimeType());
        items.append(item);
        qCDebug(LOG_GITREPORESOURCE) << "item" << item.remoteId();
    }
    git_revwalk_free(walker);
    git_repository_free(repo);

    itemsRetrieved(items);
}

bool GitRepoResource::retrieveItems(const Akonadi::Item::List &items, const QSet<QByteArray> &parts)
{
    Q_UNUSED(parts);
    // TODO: creating a repo each time might be expensive. perhaps the repo could be kept around until shutdown?
    git_repository * repo = nullptr;
    git_repository_open(&repo, repoDirectoryPath().toLocal8Bit().constData());

    Akonadi::Item::List newItems;
    newItems.reserve(items.size());

    for (const auto &item : items) {
        Akonadi::Item newItem(item);

        if (!setPayload(&newItem, repo)) {
            git_repository_free(repo);
            return false;
        }

        newItems.append(newItem);
    }

    itemsRetrieved(newItems);

    git_repository_free(repo);

    return true;
}

bool GitRepoResource::retrieveItem(const Akonadi::Item &item, const QSet<QByteArray> &parts)
{
    Q_UNUSED(parts);

    // TODO: creating a repo for each item might be expensive. perhaps the repo could be kept around until shutdown?
    git_repository * repo = nullptr;
    git_repository_open(&repo, repoDirectoryPath().toLocal8Bit().constData());

    Akonadi::Item newItem(item);

    if (!setPayload(&newItem, repo)) {
        git_repository_free(repo);
        return false;
    }

    itemRetrieved(newItem);

    git_repository_free(repo);

    return true;
}

bool GitRepoResource::setPayload(Akonadi::Item *item, git_repository *repo)
{
    git_commit * commit = nullptr;

    const QString remoteId = item->remoteId();

    git_oid oid;

    if (git_oid_fromstrp(&oid,remoteId.toLocal8Bit().constData()) == 0)  {
        git_commit_lookup(&commit, repo, &oid);
    }
    if (!commit) {
        Q_EMIT error(i18n("Commit with uid '%1' not found.", remoteId));
        git_repository_free(repo);
        return false;
    }

    KCalendarCore::Event::Ptr event(new KCalendarCore::Event());
    event->setDtStart(QDateTime::fromMSecsSinceEpoch(git_commit_time(commit) * 1000));
    event->setSummary(QString::fromLocal8Bit(git_commit_summary(commit)), -1);
    event->setDescription(QString::fromLocal8Bit(git_commit_message(commit)), -1);

    item->setPayload(event);

    git_commit_free(commit);

    return true;
}

void GitRepoResource::aboutToQuit()
{
    // TODO: code copied over, but is it needed?
    Settings::self()->save();
}


QString GitRepoResource::repoDirectoryPath() const
{
    return Settings::self()->path();
}

AKONADI_RESOURCE_MAIN(GitRepoResource)
