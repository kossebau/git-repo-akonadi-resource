/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef GITREPORESOURCE_H
#define GITREPORESOURCE_H

#include <AkonadiAgentBase/ResourceBase>

#include <git2/types.h>

class GitRepoResource : public Akonadi::ResourceBase,
                        public Akonadi::AgentBase::Observer
{
    Q_OBJECT

public:
    explicit GitRepoResource(const QString &id);
    ~GitRepoResource() override;

protected Q_SLOTS:
    void retrieveCollections() override;
    void retrieveItems(const Akonadi::Collection &col) override;
    bool retrieveItems(const Akonadi::Item::List &items, const QSet<QByteArray> &parts) override;
    bool retrieveItem(const Akonadi::Item &item, const QSet<QByteArray> &parts) override;

protected:
    void aboutToQuit() override;

private Q_SLOTS:
    void reloadConfig();

private:
    QString repoDirectoryPath() const;
    bool setPayload(Akonadi::Item *item, git_repository *repo);
};

#endif
